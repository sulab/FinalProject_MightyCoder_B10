<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Sign In Page</title>

        <!-- Bootstrap -->
        <link href="resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="resource/css/custom.css" >
        <link rel="stylesheet" type="text/css" href="resource/css/responsive.css" >
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">           
            <div class="row">               
                <div class="col-md-4 col-sm-4 col-xs-4"></div>
                <div class="col-md-4 col-sm-4 col-xs-4"></div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <h4 class="new">Not Sign In Yet? <a href="views/registration.php" class="btn btn-default signup">Sign Up</a></h4>
                    <form class="form-horizontal" role="form" action="views/OtherReleventInformations/login.php" method="post">
                        <div class="form-group">
                            <label for="email"><h3>Email address</h3></label>
                            <input type="email" class="form-control" name="email" placeholder="Write Your Email" required="required">
                        </div>

                        <div class="form-group password">
                            <label for="exampleInputPassword1"><h3>Password</h3></label>
                            <input type="password" class="form-control" name="password" placeholder="Write Your Password" required="required">
                        </div>

                        <button type="submit" class="btn btn-default submit">Sign In</button>
                    </form>
                </div>

            </div>
        </div>    

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="resource/js/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="resource/js/bootstrap.min.js"></script>
    </body>
</html>